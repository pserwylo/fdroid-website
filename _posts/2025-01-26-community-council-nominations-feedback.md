---
layout: post
title: "Seeking feedback on Community Council nominations, and a call for more"
author: "pserwylo"
---

The original [call for community council](https://f-droid.org/en/2024/12/01/call-for-community-council-nominations.html) and subsequent [extension](https://f-droid.org/en/2024/12/14/community-council-nominations-extension.html) has yielded one nomination.

In an important step toward ensuring a safe, constructive, and empowered community, the F-Droid Board put out a [call for Community Council nominations](https://f-droid.org/en/2024/12/01/call-for-community-council-nominations.html) and subsequent [extension](https://f-droid.org/en/2024/12/14/community-council-nominations-extension.html). This search yielded one nomination, that of Vishal Bakhai, aka vdbhb59 (Harry), who is an active moderator of the F-Droid forum and other channels.

If you have any feedback about the nomination of Vishal, or feedback about the process of the Community Council, please email elections {at} f-droid {dot} org before YYYY-MM-DD. We'll also try to monitor other channels on a best effort basis. 

If you would like to nominate yourself or somebody else as a potential member of the Community Council, we welcome nominations until the end of the feedback period on YYYY-MM-DD.
